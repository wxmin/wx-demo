// demo/demo.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    animationData:{},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },


  capture: function () {
    var flag = wx.canIUse('chooseVideo.object.maxDuration')
    console.log(flag)
    wx.chooseVideo({
      sourceType: [ 'camera'],
      maxDuration: 10,
      camera: 'front',
      success: function (res) {
        that.setData({
          src: res.tempFilePath
        })
      }
    })
  },

  animate: function(){
    var animation = wx.createAnimation({
      duration: 1000,
      timingFunction: 'ease',
    })
    var _this = this;
    setInterval(()=>{
      animation.translate(30).step()
      // animation.scale(2,3).rotate(45).step()
      _this.setData({ animationData:animation.export()})
    },1000)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})