// index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    flag:0,
    imgWidth:0,
    imgHeight:0,
    fontLeft:0,
    fontTop:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._login()
    this._setTime()//测试promise延时
    this._initCanvas()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  _login: function(){
    var loginPromise = new Promise((resolve,reject)=>{
      wx.login({
        success: (res) => {
          console.log(res)
          if (res.code) {
            resolve(res.code)
          }else{
            reject(res.errMsg)
          }
        },
        fail: (e) => {
          reject(e)
        },
      })
    })
    
    loginPromise().then((code)=>{
      wx.request({
        url: 'https://test',
        // https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code
        data: {
          code: code
        },
        success: (res)=>{

        },
        fail: (e)=>{

        }
      })
    })
    console.log('login---',this.data.flag)
  },
  // 封装延时函数
  _setTime: function(){
    var setTime = (time) => {
      return new Promise((resolve, reject) => {
        if (this.data.flag < 3) {
          setTimeout(resolve, time);
          console.log(this.data.flag)
          var flag = this.data.flag + 1;
          this.setData({ flag: flag })
          // this.data.flag +=1;
        } else {
          reject(new Error('超啦,大于5啦'));
        }
      })
    }

    setTime(2000).then(() => {
      return setTime(2000)
    }).then(() => {
      return setTime(2000)
    }).then(() => {
      return setTime(3000)
    }, (err) => {
      console.log(err)
    }).catch((error) => {
      console.log(error)
    })
  },

  _initCanvas: function(){
    var context = wx.createCanvasContext('test-canvas')
    this.setData({
      context,
      imgWidth: wx.getSystemInfoSync().windowWidth,
      imgHeight: wx.getSystemInfoSync().windowHeight,
    })
    context.drawImage('../img/2.jpg', 0, 0, this.data.imgWidth,this.data.imgHeight)
    // context.draw()
    var fontDraw = (i)=>{
      var text = `hello kitty!`
      context.setFontSize(18)
      context.setFillStyle("#f2721c")
      context.fillText(text, this.data.imgWidth * 0.23, this.data.imgHeight * 0.40 + i * 20)
      context.stroke();
    }
    for(var i=0;i<8;i++){
      fontDraw(i)
    }
    context.draw()
  },
  
  canvasBegin: function(e){
    console.log(e)
  },
  canvasMove: function(e){
    var context = wx.createCanvasContext('test-canvas')
    console.log('move',e)

    context.beginPath()
    context.setLineWidth(20)
    context.setLineCap('round')
    context.moveTo(10, 10)
    context.lineTo(e.touches[0].x, e.touches[0].y)
    context.stroke()

    // context.fillText('hello kitty',e.touches[0].x,e.touches[0].y)
    // context.draw()
  },


  canvasEnd: function(e){
    console.log(e)
  },
  canvasError: function(e){
    console.log(e)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})